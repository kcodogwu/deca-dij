Fill:  arabic.txt arabic16.txt arabicISO.txt arabicuu.txt chinese.txt chinese16.txt chineseGB.txt chineseuu.txt farsi.txt farsi16.txt farsi64.txt fill.dd hebrew.txt hebrew16.txt japanese.txt japanese16.txt japaneseJIS.txt random.dd russian.txt russian16.txt russian16BE.txt russian16LE.txt russian64.txt z.dd zero.dd
=======================
Files:  SPQR.JPG amalfi.bmp bamboo-clump-anamated.gif barn.gif blini.gif boudicca.bmp cactus.png cave.png cutty-sark.JPG eggs.gif forsythia.png gallop.tiff injera.gif iris-lavender.bmp iris-white.tiff iris-yellow.bmp jack-o-lantern.tiff jump.jpg lavender.png leaf.jpg log.png oak-snow.jpg orchid.png piazza-dei-miracoli.JPG pink-rose.bmp pisa.JPG river.png shoot.bmp sliced-tomatoes.tiff slices.tiff smoked-chicken.bmp stonehenge.JPG tapas.gif tomatoes.gif trail.png tulip-red.tiff wat.gif winter-street.tiff yuck.tiff zen.bmp
=======================
SPQR.JPG followed by 0 sectors of arabic.txt
amalfi.bmp followed by 1 sectors of arabic16.txt
bamboo-clump-anamated.gif followed by 2 sectors of arabicISO.txt
barn.gif followed by 4 sectors of arabicuu.txt
blini.gif followed by 8 sectors of chinese.txt
boudicca.bmp followed by 16 sectors of chinese16.txt
cactus.png followed by 32 sectors of chineseGB.txt
cave.png followed by 64 sectors of chineseuu.txt
cutty-sark.JPG followed by 128 sectors of farsi.txt
eggs.gif followed by 0 sectors of farsi16.txt
forsythia.png followed by 1 sectors of farsi64.txt
gallop.tiff followed by 2 sectors of fill.dd
injera.gif followed by 4 sectors of hebrew.txt
iris-lavender.bmp followed by 8 sectors of hebrew16.txt
iris-white.tiff followed by 16 sectors of japanese.txt
iris-yellow.bmp followed by 32 sectors of japanese16.txt
jack-o-lantern.tiff followed by 64 sectors of japaneseJIS.txt
jump.jpg followed by 128 sectors of random.dd
lavender.png followed by 0 sectors of russian.txt
leaf.jpg followed by 1 sectors of russian16.txt
log.png followed by 2 sectors of russian16BE.txt
oak-snow.jpg followed by 4 sectors of russian16LE.txt
orchid.png followed by 8 sectors of russian64.txt
piazza-dei-miracoli.JPG followed by 16 sectors of z.dd
pink-rose.bmp followed by 32 sectors of zero.dd
pisa.JPG followed by 64 sectors of arabic.txt
river.png followed by 128 sectors of arabic16.txt
shoot.bmp followed by 0 sectors of arabicISO.txt
sliced-tomatoes.tiff followed by 1 sectors of arabicuu.txt
slices.tiff followed by 2 sectors of chinese.txt
smoked-chicken.bmp followed by 4 sectors of chinese16.txt
stonehenge.JPG followed by 8 sectors of chineseGB.txt
tapas.gif followed by 16 sectors of chineseuu.txt
tomatoes.gif followed by 32 sectors of farsi.txt
trail.png followed by 64 sectors of farsi16.txt
tulip-red.tiff followed by 128 sectors of farsi64.txt
wat.gif followed by 0 sectors of fill.dd
winter-street.tiff followed by 1 sectors of hebrew.txt
yuck.tiff followed by 2 sectors of hebrew16.txt
zen.bmp followed by 4 sectors of japanese.txt
