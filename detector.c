/*
 * detector.c
 *
 *  Created on: 20 Jun 2015
 *      Author: Pavel Gladyshev
 *
 *  Detector of information type contained in the given cluster.
 *  Used to decide if the given cluster is relevant / irrelevant during
 *  probing and carving steps of Decision-theoretic file Carver (DeCa)
 */

//#include <magic.h>
//#include <linear.h>
//#include <libsvm/svm.h>
#include <string.h>
#include "deca.h"
#include "detector.h"

int deca_detector_init(Deca_detector *d, char *modelfile)
{

#ifdef DECA_SVM_JPEG_DETECTION
	/* Load appropriate model file */
	d->m = LOAD_MODEL(modelfile);
	if (d->m == NULL)
	{
		return DECA_MODEL_FILE_NOT_FOUND;
	}

#else
	// Initialise libmagic
	/*
	d->cookie = magic_open(MAGIC_MIME_TYPE);
	if (d->cookie == NULL)
	{
		return DECA_FAIL;
	}
	if(magic_load(d->cookie, NULL) != 0)
	{
		return DECA_FAIL;
	}
	*/
#endif

	return DECA_OK;
}

int deca_detector_tst_jpeg_header(Deca_detector *d, char *buf, int len)
{
   if ((buf[0]=='\xff')&&
	   (buf[1]=='\xd8')&&
	   (buf[2]=='\xff'))
   {
	   if (buf[3]=='\xe1')
	   {
		   return 1;
	   }
	   else
	   {
		   if ((buf[3]=='\xe0')&&(buf[4]=='\x00')&&(buf[5]=='\x10'))
		   {
			   return 1;
		   }
		   else
		   {
			   return 0;
		   }
	   }
   }
   else
   {
	   return 0;
   }
}

int deca_detector_tst_jpeg_footer(Deca_detector *d, char *buf, int len, int lastbyte)
{
	int i;
	for(i=len-1;i>1;i--)
	{
		if(buf[i]!='\xff')
		{
  	  	  if(buf[i-1]=='\xd9')
		  {
			if(buf[i-2]=='\xff')
			{
				return i;
			}
		  }
		}
	}
	if ((buf[1]!='\xff') && (buf[0]=='\xd9') && (lastbyte == '\xff'))
	{
		return 1;
	}
	else
	{
	    return 0;
	}
}

int deca_detector_tst_jpeg_data(Deca_detector *d, char *buf, int len)
{

    int i;
    int f[256];
    int esc_count;

#ifdef DECA_SVM_JPEG_DETECTION
    int j;
    double h[256];
    NODE features[1024];  /* Input data is a vector of byte value frequencies */

	//if (deca_detector_tst_jpeg_header(d,buf,len)) return 1;

    /* Initialise frequency array */
    for (i=0; i<256; i++)
    {
    	h[i] = 0.0;
    }

    /* Calculate frequencies of byte values in the buffer */
    for (i=0; i<len; i++)
    {
    	h[(unsigned char)(buf[i])] += 1.0;
    }
    /* Implement scaling here */
    for (i=0; i<256; i++)
    {
    	h[i] = h[i] / (len);
    }
    /* Prepare array of features */
    for (i=0,j=0; i< 256; i++)
    {
        if (h[i] != 0)
        {
        	features[j].index = i+1;
        	features[j].value = h[i];
        	j++;
        }
    }
    features[j].index = -1;
    /* Get prediction on svm model. Return "true" if the prediction is in (0.. +1] or
     * "false" if the prediction is in [-1..0] */
    return (PREDICT(d->m,features))>0;

#else

    /* Initialise frequency array */
    for(i=0; i<256; i++)
    {
    	f[i]=0;
    }

    /* reset counter of reset and escape sequences */
    esc_count=0;

    /* Calculate frequencies of byte values in the buffer */
    for (i=0; i<len-1; i++)
    {
       if ( (f[(unsigned char)(buf[i])] += 1) > 50 ) return 0;
       if (((unsigned char)buf[i])==0xff)
       {
    	   if ( (((unsigned char)buf[i+1])==0) || ((((unsigned char)buf[i+1]) & 0xF8)==0xD0) )
    	   {
    		   esc_count++;
    	   }
    	   else
    	   {
    		   if (((unsigned char)buf[i+1])==0xff)
    		   {
    			   return 0;
    		   }
    	   }
       }
       //if (esc_count != 0) return 1;
    }

    return (esc_count != 0);

	/* JPEG detection using Libmagic */
	/*char *descr = magic_buffer(d->cookie,buf,len);
	if (descr == NULL)
	{
		return 0;
	}
	if (strcmp(descr,"application/octet-stream")==0)
	{
		return 1;
	}
	else
	{
		return 0;
	}*/

#endif
}

int deca_detector_tst_png_header(Deca_detector *d, char *buf, int len) {
  if (
		((unsigned char) buf[0] == 0x89) && ((unsigned char) buf[1] == 0x50)
		&& ((unsigned char) buf[2] == 0x4e) && ((unsigned char) buf[3] == 0x47)
		&& ((unsigned char) buf[4] == 0x0d) && ((unsigned char) buf[5] == 0x0a)
		&& ((unsigned char) buf[6] == 0x1a) && ((unsigned char) buf[7] == 0x0a)
  ) {
		return 1;
  } else {
		return 0;
  }
}

int deca_detector_tst_png_footer(Deca_detector *d, char *buf, int len, int lastbyte) {
  int i;
  
  for (i = len-1; i > 12; i--) {
    if (
			((unsigned char) buf[i] == 0x82) && ((unsigned char) buf[i - 1] == 0x60)
			&& ((unsigned char) buf[i - 2]== 0x42) && ((unsigned char) buf[i - 3]== 0xae)
			&& ((unsigned char) buf[i - 4]== 0x44) && ((unsigned char) buf[i - 5]== 0x4e)
			&& ((unsigned char) buf[i - 6]== 0x45) && ((unsigned char) buf[i - 7]== 0x49)
			&& ((unsigned char) buf[i - 8]== 0x00) && ((unsigned char) buf[i - 9]== 0x00)
			&& ((unsigned char) buf[i - 10]== 0x00) && ((unsigned char) buf[i - 11]== 0x00)
    ) {
			return 1;
		}
	}
  
	if ((
	  ((unsigned char) buf[0]==0x00) && ((unsigned char) buf[1]==0x00)
	  && ((unsigned char) buf[2]==0x00) && ((unsigned char) buf[3]==0x00)
		&& ((unsigned char) buf[4]==0x49) && ((unsigned char) buf[5]==0x45)
		&& ((unsigned char) buf[6]==0x4e) && ((unsigned char) buf[7]==0x44)
		&& ((unsigned char) buf[8]==0xae) && ((unsigned char) buf[9]==0x42)
	  && ((unsigned char) buf[10] ==0x60) && ((unsigned char) buf[11] ==0x82)
		&& ((unsigned char) buf[12] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x00)
	  && ((unsigned char) buf[1]==0x00) && ((unsigned char) buf[2]==0x00)
		&& ((unsigned char) buf[3]==0x49) && ((unsigned char) buf[4]==0x45)
		&& ((unsigned char) buf[5]==0x4e) && ((unsigned char) buf[6]==0x44)
		&& ((unsigned char) buf[7]==0xae) && ((unsigned char) buf[8]==0x42)
	  && ((unsigned char) buf[9] ==0x60) && ((unsigned char) buf[10] ==0x82)
		&& ((unsigned char) buf[11] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x00) && ((unsigned char) buf[1]==0x00)
		&& ((unsigned char) buf[2]==0x49) && ((unsigned char) buf[3]==0x45)
		&& ((unsigned char) buf[4]==0x4e) && ((unsigned char) buf[5]==0x44)
		&& ((unsigned char) buf[6]==0xae) && ((unsigned char) buf[7]==0x42)
	  && ((unsigned char) buf[8] ==0x60) && ((unsigned char) buf[9] ==0x82)
		&& ((unsigned char) buf[10] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x00)
		&& ((unsigned char) buf[1]==0x49) && ((unsigned char) buf[2]==0x45)
		&& ((unsigned char) buf[3]==0x4e) && ((unsigned char) buf[4]==0x44)
		&& ((unsigned char) buf[5]==0xae) && ((unsigned char) buf[6]==0x42)
	  && ((unsigned char) buf[7] ==0x60) && ((unsigned char) buf[8] ==0x82)
		&& ((unsigned char) buf[9] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x49) && ((unsigned char) buf[1]==0x45)
		&& ((unsigned char) buf[2]==0x4e) && ((unsigned char) buf[3]==0x44)
		&& ((unsigned char) buf[4]==0xae) && ((unsigned char) buf[5]==0x42)
	  && ((unsigned char) buf[6] ==0x60) && ((unsigned char) buf[7] ==0x82)
		&& ((unsigned char) buf[8] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x45)
		&& ((unsigned char) buf[1]==0x4e) && ((unsigned char) buf[2]==0x44)
		&& ((unsigned char) buf[3]==0xae) && ((unsigned char) buf[4]==0x42)
	  && ((unsigned char) buf[5] ==0x60) && ((unsigned char) buf[6] ==0x82)
		&& ((unsigned char) buf[7] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x4e) && ((unsigned char) buf[1]==0x44)
		&& ((unsigned char) buf[2]==0xae) && ((unsigned char) buf[3]==0x42)
	  && ((unsigned char) buf[4] ==0x60) && ((unsigned char) buf[5] ==0x82)
		&& ((unsigned char) buf[6] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x44)
		&& ((unsigned char) buf[1]==0xae) && ((unsigned char) buf[2]==0x42)
	  && ((unsigned char) buf[3] ==0x60) && ((unsigned char) buf[4] ==0x82)
		&& ((unsigned char) buf[5] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0xae) && ((unsigned char) buf[1]==0x42)
	  && ((unsigned char) buf[2] ==0x60) && ((unsigned char) buf[3] ==0x82)
		&& ((unsigned char) buf[4] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0]==0x42)
	  && ((unsigned char) buf[1] ==0x60) && ((unsigned char) buf[2] ==0x82)
		&& ((unsigned char) buf[3] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0] ==0x60) && ((unsigned char) buf[1] ==0x82)
		&& ((unsigned char) buf[2] !=0xff) && ((unsigned char) lastbyte == 0xff)
	) || (
	  ((unsigned char) buf[0] ==0x82)
		&& ((unsigned char) buf[1] !=0xff) && ((unsigned char) lastbyte == 0xff)
	)) {
    return 1;
  } else {
    return 0;
  }
}

int deca_detector_tst_png_data(Deca_detector *d, char *buf, int len) {
	int i;

	for (i = 0; i < len - 1; i++) {
		if (((unsigned char) buf[i]) == 0x49) { // I
			if (
				(((unsigned char) buf[i + 1]) == 0x48) && (((unsigned char) buf[i + 2]) == 0x44) 
				&& (((unsigned char) buf[i + 3]) == 0x52)
			) { // HDR >> IHDR
				return 1;
			} else if (
				(((unsigned char) buf[i + 1]) == 0x44) && (((unsigned char) buf[i + 2]) == 0x41) 
				&& (((unsigned char) buf[i + 3]) == 0x54)
			) { // DAT >> IDAT
				return 1;
			} else if (
				(((unsigned char) buf[i + 1]) == 0x45) && (((unsigned char) buf[i + 2]) == 0x4e)
				&& (((unsigned char) buf[i + 3]) == 0x44)
			) { // END >> IEND
				return 1;
			}
		} else if (((unsigned char) buf[i]) == 0x74) { // t
			if (
				(((unsigned char) buf[i + 1]) == 0x52) && (((unsigned char) buf[i + 2]) == 0x4e) 
				&& (((unsigned char) buf[i + 3]) == 0x53)
			) { // RNS >> tRNS
				return 1;
			} else if (
				(((unsigned char) buf[i + 1]) == 0x45) && (((unsigned char) buf[i + 2]) == 0x58) 
				&& (((unsigned char) buf[i + 3]) == 0x74)
			) { // EXt >> tEXt
				return 1;
			} else if (
				(((unsigned char) buf[i + 1]) == 0x49) && (((unsigned char) buf[i + 2]) == 0x4d) 
				&& (((unsigned char) buf[i + 3]) == 0x45)
			) { // IME >> tIME
				return 1;
			}
		} else if (((unsigned char) buf[i]) == 0x73) { // s
			if (
				(((unsigned char) buf[i + 1]) == 0x42) && (((unsigned char) buf[i + 2]) == 0x49) 
				&& (((unsigned char) buf[i + 3]) == 0x54)
			) { // BIT >> sBIT
				return 1;
			} else if (
				(((unsigned char) buf[i + 1]) == 0x52) && (((unsigned char) buf[i + 2]) == 0x47) 
				&& (((unsigned char) buf[i + 3]) == 0x42)
			) { // RGB >> sRGB
				return 1;
			} else if (
				(((unsigned char) buf[i + 1]) == 0x50) && (((unsigned char) buf[i + 2]) == 0x4c) 
				&& (((unsigned char) buf[i + 3]) == 0x54)
			) { // PLT >> sPLT
				return 1;
			} else if (
				(((unsigned char)buf[i + 1]) == 0x54) && (((unsigned char)buf[i + 2]) == 0x45) 
				&& (((unsigned char)buf[i + 3]) == 0x52)
			) { // TER >> sTER
				return 1;
			}
		} else if (
			(((unsigned char) buf[i + 1]) == 0x54) && (((unsigned char) buf[i + 2]) == 0x58) 
			&& (((unsigned char) buf[i + 3]) == 0x74)
		) { // TXt
			if (((unsigned char) buf[i]) == 0x7a) { // z >> zTXt
				return 1;
			} else if (((unsigned char) buf[i]) == 0x69) { // i >> iTXt
				return 1;
			}
		} else if (
			(((unsigned char)buf[i]) == 0x67) && (((unsigned char)buf[i + 1]) == 0x49)
			&& (((unsigned char)buf[i + 2]) == 0x46)
		) { // gIF
			if (((unsigned char)buf[i + 3]) == 0x67) { // g >> gIFg
				return 1;
			} else if (((unsigned char)buf[i + 3]) == 0x74) { // t >> gIFt
				return 1;
			} else if (((unsigned char)buf[i + 3]) == 0x78) { // x >> gIFx
				return 1;
			}
		} else if (
			(((unsigned char)buf[i + 1]) == 0x43) && (((unsigned char)buf[i + 2]) == 0x41) 
			&& (((unsigned char)buf[i + 3]) == 0x4c)
		) { // CAL
			if (((unsigned char)buf[i]) == 0x70) { // p >> pCAL
				return 1;
			} else if (((unsigned char)buf[i]) == 0x73) { // s >> sCAL
				return 1;
			}
		} else if (
			((((unsigned char) buf[i]) == 0x50) && (((unsigned char) buf[i + 1]) == 0x4c)
			&& (((unsigned char) buf[i + 2]) == 0x54) && (((unsigned char) buf[i + 3]) == 0x45)) || // PLTE
			((((unsigned char) buf[i]) == 0x63) && (((unsigned char) buf[i + 1]) == 0x48)
			&& (((unsigned char) buf[i + 2]) == 0x52) && (((unsigned char) buf[i + 3]) == 0x4d)) || // cHRM
			((((unsigned char) buf[i]) == 0x67) && (((unsigned char) buf[i + 1]) == 0x41)
			&& (((unsigned char) buf[i + 2]) == 0x4d) && (((unsigned char) buf[i + 3]) == 0x41)) || // gAMA
			((((unsigned char) buf[i]) == 0x69) && (((unsigned char) buf[i + 1]) == 0x43)
			&& (((unsigned char) buf[i + 2]) == 0x43) && (((unsigned char) buf[i + 3]) == 0x50)) || // iCCP
			((((unsigned char) buf[i]) == 0x62) && (((unsigned char) buf[i + 1]) == 0x4b)
			&& (((unsigned char) buf[i + 2]) == 0x47) && (((unsigned char) buf[i + 3]) == 0x44)) || // bKGD
			((((unsigned char) buf[i]) == 0x68) && (((unsigned char) buf[i + 1]) == 0x49)
			&& (((unsigned char) buf[i + 2]) == 0x53) && (((unsigned char) buf[i + 3]) == 0x54)) || // hIST
			((((unsigned char) buf[i]) == 0x70) && (((unsigned char) buf[i + 1]) == 0x48)
			&& (((unsigned char) buf[i + 2]) == 0x59) && (((unsigned char) buf[i + 3]) == 0x73)) || // pHYs
			((((unsigned char) buf[i]) == 0x66) && (((unsigned char) buf[i + 1]) == 0x52)
			&& (((unsigned char) buf[i + 2]) == 0x41) && (((unsigned char) buf[i + 3]) == 0x63)) || // fRAc
			((((unsigned char) buf[i]) == 0x69) && (((unsigned char) buf[i + 1]) == 0x46)
			&& (((unsigned char) buf[i + 2]) == 0x46) && (((unsigned char) buf[i + 3]) == 0x73)) || // oFFs
			((((unsigned char) buf[i]) == 0x64) && (((unsigned char) buf[i + 1]) == 0x53)
			&& (((unsigned char) buf[i + 2]) == 0x49) && (((unsigned char) buf[i + 3]) == 0x47)) || // dSIG
			((((unsigned char) buf[i]) == 0x65) && (((unsigned char) buf[i + 1]) == 0x58)
			&& (((unsigned char) buf[i + 2]) == 0x65) && (((unsigned char) buf[i + 3]) == 0x66)) // eXIf
		) { 
			return 1;
		}
	}
	
	return 0;
}

int deca_detector_close(Deca_detector *d)
{
	//magic_close(d->cookie);
	return DECA_OK;
}
